# Выполненное тестовое задание для Регеора Девелопмент

Проект поднят на Composer, из библиотек использовал только `nesbot/carbon` 
(привычка уже). 

Для dev-окружения: `friendsofphp/php-cs-fixer` в качестве cleaner 
по кодстайлу, `robmorgan/phinx` для миграции и `codeception/codeception` 
для тестирования.

## Установка и запуск приложения

Я отправил приглашение для доступа к репозиторию на `ek@regeora.ru`.

1. Создаём локально пустую базу данных:

    ```sql
    CREATE DATABASE `raspvariant` COLLATE 'utf8_general_ci';
    ```

2. Скачиваем репозиторий и запускаем composer:

    ```bash
    git clone git@gitlab.com:mosiyash/raspvariant.git
    cd raspvariant
    composer install
    ```

3. В `app/config/params-local.php` настраиваем параметры подключения к БД 
в соответствии с дефолтными, которые находятся в файле `app/config/params.php`.

4. Делаем миграцию и применяем сидер (нужно для п. 4 из ТЗ). 

    В команду Финксу нужно передать свои параметры подключения. Пример ниже — мой.

    ```bash
    export PHINX_DBHOST=localhost PHINX_DBNAME=raspvariant PHINX_DBUSER=root PHINX_DBPASS=root
    vendor/bin/phinx migrate
    vendor/bin/phinx seed:run
    ```

5. Запуск тестов:

    ```bash
    vendor/bin/codeception run
    ```
    
Если есть неточности по выполнению ТЗ, прошу сказать об этом. И если по коду есть вопросы, их тоже можно задавать :)

## Оценка трудозатрат

1. *Распарсить xml в классы для работы с данными. Каждый “выход” должен 
быть отдельным объектом, содержащий в себе массив смен. То есть объединять 
<graph> с одинаковым num, смены должны быть в порядке возрастания их номера.*

    На первом этапе я начал с базовой Composer-структуры проекта и подключения 
    тестов. Задание старался делать по TDD, это как раз именно тот случай,
    когда и применение удобно, и время на это есть :)

    **5 часов**

2. *Реализовать метод у класса “выхода“, возвращающий количество производственных рейсов, 
общее время производственных рейсов (сумма всех end-start).*

    **10 минут**

3. *Реализовать метод у класса “выхода“, принимающий на вход “время с” и “время по“, 
возвращающий все остановки всех производственных рейсов “выхода“, 
попадающих в этот временной диапазон. Результат должен быть в таком виде, 
чтобы у каждой записи можно было определить номер смены и порядковый 
номер рейса в смене (добавление ли это данных в объект или какая-то 
отдельная возвращаемая структура - решайте сами).*

    У меня это метод объекта.
    
    **30 минут**
    
4. *Реализовать таблицу в БД StopPoints с id (внутренний идентификатор), 
external_id (внешний идентификатор), name (наименование). 
Сделать чтобы метод из пункта 3 возвращал так же названия остановок 
по их external_id.*

    Надеюсь я правильно понял, что `id` это autoincrement, и `external_id` это значение 
    параметра `st_id`.
    
    Тут я подключил Phinx, создал миграцию и сидер. Ну и сам метод.
    
    **1 час**
    
5. *Написал README.md*

    **40 мин** :)