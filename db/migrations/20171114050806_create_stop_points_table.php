<?php


use Phinx\Migration\AbstractMigration;

class CreateStopPointsTable extends AbstractMigration
{
    public function up()
    {
        $this
            ->table('stop_points')
            ->addColumn('external_id', 'integer')
            ->addColumn('name', 'string')
            ->addIndex(['external_id'], ['unique' => true])
            ->create();
    }

    public function down()
    {
        $this->dropTable('stop_points');
    }
}
