<?php


use Phinx\Seed\AbstractSeed;
use Raspvariant\Raspvariant;
use Raspvariant\Service;

class StopPointsSeeder extends AbstractSeed
{
    public function run()
    {
        $data = [];

        $filepath = dirname(dirname(__DIR__)).'/var/raspvariant.xml';
        $xmlString = file_get_contents($filepath);
        $raspvariant = new Raspvariant($xmlString);

        foreach ($raspvariant->getGraphs() as $graph) {
            foreach ($graph->getShifts() as $shift) {
                foreach ($shift->getProductionEvents() as $event) {
                    foreach ($event->getStops() as $stop) {
                        if (!array_key_exists($stop->getStId(), $data)) {
                            $data[$stop->getStId()] = [
                                'external_id' => $stop->getStId(),
                                'name'        => 'Остановка #'.$stop->getStId(),
                            ];
                        }
                    }
                }
            }
        }

        $this
            ->table('stop_points')
            ->insert(array_values($data))
            ->save();
    }
}
