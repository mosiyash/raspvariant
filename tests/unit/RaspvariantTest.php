<?php


use Carbon\Carbon;
use Raspvariant\Event;
use Raspvariant\Graph;
use Raspvariant\Raspvariant;
use Raspvariant\Shift;
use Raspvariant\Stop;
use Raspvariant\StopEvent;

class RaspvariantTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testXmlTransformer()
    {
        $filepath    = dirname(dirname(__DIR__)).'/var/raspvariant.xml';
        $xmlString   = file_get_contents($filepath);
        $raspvariant = new Raspvariant($xmlString);

        $this->assertInstanceOf(Raspvariant::class, $raspvariant);
        $this->assertEquals(Carbon::parse('2017-03-16 10:29:57'), $raspvariant->getSnapTime());

        $this->assertCount(6, $raspvariant->getGraphs());

        $graph = $raspvariant->getGraph(1);
        $this->assertInstanceOf(Graph::class, $graph);
        $this->assertSame(1, $graph->getNum());
        $this->assertCount(2, $graph->getShifts());

        $stopsByPeriod = $graph->getProductionStopsByPeriod(Carbon::parse('2017-01-09 04:45:00'), Carbon::parse('2017-01-09 04:59:59'));
        $this->assertCount(10, $stopsByPeriod);
        $this->assertSame('Остановка #50280388', $stopsByPeriod[0]->getStName());
        unset($stopsByPeriod);

        $shift = $graph->getShifts()[1];
        $this->assertInstanceOf(Shift::class, $shift);
        $this->assertCount(26, $shift->getEvents());
        $this->assertCount(13, $shift->getProductionEvents());
        $this->assertSame(13, $shift->getProductionFlightsCount());
        $this->assertSame(332, $shift->getProductionFlightsTotalDuration());

        $event = $shift->getEvent(0);
        $this->assertInstanceOf(Event::class, $event);
        $this->assertEquals(3, $event->getEvId());
        $this->assertSame('04:15', $event->getStart());
        $this->assertSame('04:45', $event->getEnd());
        $this->assertSame(50970522, $event->getDepartureId());
        $this->assertSame(50280388, $event->getArrivalId());
        $this->assertSame(3.276, $event->getDistance());
        $this->assertSame(30, $event->getDuration());

        unset($event);

        /** @var StopEvent $event */
        $event = $shift->getEvent(1);
        $this->assertInstanceOf(StopEvent::class, $event);
        $this->assertEquals(4, $event->getEvId());
        $this->assertCount(15, $event->getStops());

        $stop = $event->getStop(0);
        $this->assertInstanceOf(Stop::class, $stop);
        $this->assertSame(50280388, $stop->getStId());
        $this->assertSame('04:45', $stop->getTime());
        $this->assertSame(0.607, $stop->getDistanceToNext());
        $this->assertSame(1, $stop->getKp());

        unset($stop, $event, $shift, $graph);
    }
}
