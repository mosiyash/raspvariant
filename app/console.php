<?php

use Raspvariant\Console\ParseXmlCommand;
use Symfony\Component\Console\Application;

require dirname(__DIR__).'/vendor/autoload.php';

$application = new Application();
$application->add(new ParseXmlCommand());
$application->run();
