<?php

namespace Raspvariant;

class Stop
{
    /**
     * @var StopEvent
     */
    private $event;

    /**
     * @var int
     */
    private $stId;

    /**
     * @var string
     */
    private $time;

    /**
     * @var float
     */
    private $distanceToNext;

    /**
     * @var int
     */
    private $kp;

    /**
     * @var string
     */
    private $stName;

    public function __construct(StopEvent $event, \SimpleXMLElement $element)
    {
        $this->event          = $event;
        $this->stId           = (int) $element->attributes()['st_id'];
        $this->time           = (string) $element->attributes()['time'];
        $this->distanceToNext = (float) $element->attributes()['distanceToNext'];
        $this->kp             = (int) $element->attributes()['kp'];
    }

    /**
     * @return StopEvent
     */
    public function getEvent(): StopEvent
    {
        return $this->event;
    }

    /**
     * @return int
     */
    public function getStId(): int
    {
        return $this->stId;
    }

    /**
     * @return null|string
     */
    public function getStName(): ?string
    {
        return $this->stName;
    }

    /**
     * @param string $stName
     */
    public function setStName(string $stName)
    {
        $this->stName = $stName;
    }

    /**
     * @return string
     */
    public function getTime(): string
    {
        return $this->time;
    }

    /**
     * @return float
     */
    public function getDistanceToNext(): float
    {
        return $this->distanceToNext;
    }

    /**
     * @return int
     */
    public function getKp(): int
    {
        return $this->kp;
    }
}
