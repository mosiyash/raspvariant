<?php

namespace Raspvariant;

class Event
{
    /**
     * @var Shift
     */
    private $shift;

    /**
     * @var int
     */
    private $evId;

    /**
     * @var string
     */
    private $start;

    /**
     * @var string
     */
    private $end;

    /**
     * @var int
     */
    private $departureId;

    /**
     * @var int
     */
    private $arrivalId;

    /**
     * @var float
     */
    private $distance;

    /**
     * @var int
     */
    private $duration;

    public function __construct(Shift $shift, \SimpleXMLElement $element)
    {
        $this->shift       = $shift;
        $this->evId        = (int) $element->attributes()['ev_id'];
        $this->start       = (string) $element->attributes()['start'];
        $this->end         = (string) $element->attributes()['end'];
        $this->departureId = (int) $element->attributes()['departureID'];
        $this->arrivalId   = (int) $element->attributes()['arrivalID'];
        $this->distance    = (float) $element->attributes()['distance'];
        $this->duration    = (int) $element->attributes()['duration'];
    }

    /**
     * @return Shift
     */
    public function getShift(): Shift
    {
        return $this->shift;
    }

    /**
     * @return int
     */
    public function getEvId(): int
    {
        return $this->evId;
    }

    /**
     * @return string
     */
    public function getStart(): string
    {
        return $this->start;
    }

    /**
     * @return string
     */
    public function getEnd(): string
    {
        return $this->end;
    }

    /**
     * @return int
     */
    public function getDepartureId(): int
    {
        return $this->departureId;
    }

    /**
     * @return int
     */
    public function getArrivalId(): int
    {
        return $this->arrivalId;
    }

    /**
     * @return float
     */
    public function getDistance(): float
    {
        return $this->distance;
    }

    /**
     * @return int
     */
    public function getDuration(): int
    {
        return $this->duration;
    }
}
