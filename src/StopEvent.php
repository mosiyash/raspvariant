<?php

namespace Raspvariant;

class StopEvent extends Event
{
    /**
     * @var Stop[]
     */
    private $stops = [];

    public function __construct(Shift $shift, \SimpleXMLElement $element)
    {
        parent::__construct($shift, $element);

        foreach ($element->stop as $xmlStop) {
            $this->stops[] = new Stop($this, $xmlStop);
        }
    }

    /**
     * @return Stop[]
     */
    public function getStops(): array
    {
        return $this->stops;
    }

    public function getStop(int $index): Stop
    {
        return $this->stops[$index];
    }
}
