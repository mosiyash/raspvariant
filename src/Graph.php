<?php

namespace Raspvariant;

use Carbon\Carbon;

class Graph
{
    /**
     * @var Raspvariant
     */
    private $raspvariant;

    /**
     * @var int
     */
    private $num;

    /**
     * @var Shift[]
     */
    private $shifts = [];

    public function __construct(Raspvariant $raspvariant, \SimpleXMLElement $element)
    {
        $this->raspvariant                                   = $raspvariant;
        $this->num                                           = (int) $element->attributes()['num'];
        $this->shifts[(int) $element->attributes()['smena']] = new Shift($this, $element);
    }

    public function append(\SimpleXMLElement $element)
    {
        $this->shifts[(int) $element->attributes()['smena']] = new Shift($this, $element);
    }

    /**
     * @return Raspvariant
     */
    public function getRaspvariant(): Raspvariant
    {
        return $this->raspvariant;
    }

    /**
     * @return int
     */
    public function getNum(): int
    {
        return $this->num;
    }

    /**
     * @return Shift[]
     */
    public function getShifts(): array
    {
        return $this->shifts;
    }

    /**
     * Замечу, что название остановок возвращаются через метод Raspvariant\Stop::getStName().
     *
     * @param Carbon $from
     * @param Carbon $to
     *
     * @return Stop[]
     */
    public function getProductionStopsByPeriod(Carbon $from, Carbon $to): array
    {
        $data = [];
        $externalIds = [];

        foreach ($this->getShifts() as $shift) {
            foreach ($shift->getProductionEvents() as $event) {
                foreach ($event->getStops() as $stop) {
                    $datetime = Carbon::parse(sprintf('%s %s:00', $this->getRaspvariant()->getStart()->format('Y-m-d'), $stop->getTime()));
                    if ($datetime->timestamp >= $from->timestamp && $datetime->timestamp <= $to->timestamp) {
                        $data[] = $stop;
                        $externalIds[] = $stop->getStId();
                    }
                }
                unset($stop);
            }
            unset($event);
        }
        unset($shift);

        $externalIds = array_unique($externalIds);
        asort($externalIds);

        if ($externalIds) {
            $stmt = Service::database()->prepare('SELECT external_id, name FROM stop_points WHERE external_id IN ('.implode(', ', $externalIds).')');
            $stmt->execute();
            $externalNames = $stmt->fetchAll(\PDO::FETCH_KEY_PAIR);
            /**
             * @var int  $dataKey
             * @var Stop $stop
             */
            foreach ($data as $dataKey => $stop) {
                if (array_key_exists($stop->getStId(), $externalNames)) {
                    $stop->setStName($externalNames[$stop->getStId()]);
                }
            }
            unset($stop);
        }

        return $data;
    }
}
