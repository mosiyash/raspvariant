<?php

namespace Raspvariant;

class Shift
{
    /**
     * @var Graph
     */
    private $graph;

    /**
     * @var int
     */
    private $num;

    /**
     * @var float
     */
    private $nullRun;

    /**
     * @var float
     */
    private $lineRun;

    /**
     * @var float
     */
    private $totalRun;

    /**
     * @var int
     */
    private $nullTime;

    /**
     * @var int
     */
    private $lineTime;

    /**
     * @var int
     */
    private $otsTime;

    /**
     * @var int
     */
    private $totalTime;

    /**
     * @var string
     */
    private $garageOut;

    /**
     * @var string
     */
    private $garageIn;

    /**
     * @var string
     */
    private $lineBegin;

    /**
     * @var string
     */
    private $lineEnd;

    /**
     * @var Event[]
     */
    private $events = [];

    public function __construct(Graph $graph, \SimpleXMLElement $element)
    {
        $this->graph = $graph;
        $this->append($element);
    }

    public function append(\SimpleXMLElement $element)
    {
        $this->num       = (int) $element->attributes()['num'];
        $this->nullRun   = (float) $element->attributes()['nullRun'];
        $this->lineRun   = (float) $element->attributes()['lineRun'];
        $this->totalRun  = (float) $element->attributes()['totalRun'];
        $this->nullTime  = (int) $element->attributes()['nullTime'];
        $this->lineTime  = (int) $element->attributes()['lineTime'];
        $this->otsTime   = (int) $element->attributes()['otsTime'];
        $this->totalTime = (int) $element->attributes()['totalTime'];
        $this->garageOut = (string) $element->attributes()['garageOut'];
        $this->garageIn  = (string) $element->attributes()['garageIn'];
        $this->lineBegin = (string) $element->attributes()['lineBegin'];
        $this->lineEnd   = (string) $element->attributes()['lineEnd'];

        foreach ($element->event as $xmlEvent) {
            $this->events[] = ((int) $xmlEvent->attributes()['ev_id'] === 4)
                ? new StopEvent($this, $xmlEvent)
                : new Event($this, $xmlEvent);
        }
    }

    /**
     * @return Graph
     */
    public function getGraph(): Graph
    {
        return $this->graph;
    }

    /**
     * @return int
     */
    public function getNum(): int
    {
        return $this->num;
    }

    /**
     * @return float
     */
    public function getNullRun(): float
    {
        return $this->nullRun;
    }

    /**
     * @return float
     */
    public function getLineRun(): float
    {
        return $this->lineRun;
    }

    /**
     * @return float
     */
    public function getTotalRun(): float
    {
        return $this->totalRun;
    }

    /**
     * @return int
     */
    public function getNullTime(): int
    {
        return $this->nullTime;
    }

    /**
     * @return int
     */
    public function getLineTime(): int
    {
        return $this->lineTime;
    }

    /**
     * @return int
     */
    public function getOtsTime(): int
    {
        return $this->otsTime;
    }

    /**
     * @return int
     */
    public function getTotalTime(): int
    {
        return $this->totalTime;
    }

    /**
     * @return string
     */
    public function getGarageOut(): string
    {
        return $this->garageOut;
    }

    /**
     * @return string
     */
    public function getGarageIn(): string
    {
        return $this->garageIn;
    }

    /**
     * @return string
     */
    public function getLineBegin(): string
    {
        return $this->lineBegin;
    }

    /**
     * @return string
     */
    public function getLineEnd(): string
    {
        return $this->lineEnd;
    }

    /**
     * @return Event[]
     */
    public function getEvents(): array
    {
        return $this->events;
    }

    /**
     * @return StopEvent[]
     */
    public function getProductionEvents(): array
    {
        $data = [];
        foreach ($this->events as $event) {
            if ($event instanceof StopEvent) {
                $data[] = $event;
            }
        }

        return $data;
    }

    public function getEvent(int $index): Event
    {
        return $this->events[$index];
    }

    public function getProductionFlightsCount(): int
    {
        $count = 0;
        foreach ($this->getProductionEvents() as $event) {
            ++$count;
        }

        return $count;
    }

    public function getProductionFlightsTotalDuration(): int
    {
        $duration = 0;
        foreach ($this->getProductionEvents() as $event) {
            $duration += $event->getDuration();
        }

        return $duration;
    }
}
