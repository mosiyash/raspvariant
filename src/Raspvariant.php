<?php

namespace Raspvariant;

use Carbon\Carbon;

class Raspvariant
{
    /**
     * @var Carbon
     */
    private $snapTime;

    /**
     * @var int
     */
    private $num;

    /**
     * @var Carbon
     */
    private $start;

    /**
     * @var null|Carbon
     */
    private $end;

    /**
     * @var int
     */
    private $dow;

    /**
     * @var int
     */
    private $mrId;

    /**
     * @var int
     */
    private $mrNum;

    /**
     * @var Graph[]
     */
    private $graphs = [];

    public function __construct(string $xmlString)
    {
        $xml = new \SimpleXMLElement($xmlString);

        $this->snapTime = Carbon::parse($xml->attributes()['snapTime']);
        $this->num      = (int) $xml->attributes()['num'];
        $this->start    = Carbon::parse($xml->attributes()['start']);
        $this->end      = ($xml->attributes()['end'] === '') ? null : Carbon::parse($xml->attributes()['end']);
        $this->dow      = (int) $xml->attributes()['dow'];
        $this->mrId     = (int) $xml->attributes()['mr_id'];
        $this->mrNum    = (int) $xml->attributes()['mr_num'];

        foreach ($xml->graph as $xmlGraph) {
            $graphNum = (int) $xmlGraph->attributes()['num'];
            if (!array_key_exists($graphNum, $this->graphs)) {
                $this->graphs[$graphNum] = new Graph($this, $xmlGraph);
            } else {
                $this->graphs[$graphNum]->append($xmlGraph);
            }
        }
    }

    /**
     * @return Carbon
     */
    public function getSnapTime(): Carbon
    {
        return $this->snapTime;
    }

    /**
     * @return int
     */
    public function getNum(): int
    {
        return $this->num;
    }

    /**
     * @return Carbon
     */
    public function getStart(): Carbon
    {
        return $this->start;
    }

    /**
     * @return Carbon|null
     */
    public function getEnd(): ? Carbon
    {
        return $this->end;
    }

    /**
     * @return int
     */
    public function getDow() : int
    {
        return $this->dow;
    }

    /**
     * @return int
     */
    public function getMrId(): int
    {
        return $this->mrId;
    }

    /**
     * @return int
     */
    public function getMrNum(): int
    {
        return $this->mrNum;
    }

    /**
     * @return Graph[]
     */
    public function getGraphs(): array
    {
        return $this->graphs;
    }

    /**
     * @param int $num
     *
     * @return Graph
     */
    public function getGraph(int $num): Graph
    {
        return $this->graphs[$num];
    }
}
