<?php

namespace Raspvariant;

class Service
{
    /**
     * @var array
     */
    private static $params = [];

    /**
     * @var
     */
    private static $database;

    public static function params(): array
    {
        if (!self::$params) {
            self::$params = require dirname(__DIR__).'/app/params.php';
            if (file_exists(dirname(__DIR__).'/app/params-local.php')) {
                self::$params = array_merge(self::$params, require dirname(__DIR__).'/app/params-local.php');
            }
        }

        return self::$params;
    }

    public static function database(): \PDO
    {
        if (self::$database === null) {
            self::$database = new \PDO(sprintf('mysql:dbname=%s;host=%s', self::params()['db_name'], self::params()['db_host']), self::params()['db_user'], self::params()['db_pass']);
            self::$database->exec('SET NAMES utf8');
        }

        return self::$database;
    }
}
